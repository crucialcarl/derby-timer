### A simple timer to be run on raspberry pi for timing 3 lane pinewood derby.  

#### I borrowed ideas or code from a lot of different places.  Many thanks to each of the original authors.


#### https://hackaday.io/project/2275-openderby
#### http://learn.parallax.com/KickStart/555-27401
#### http://www.instructables.com/id/Arduino-Pinewood-Derby-Timer/
#### https://www.sqlite.org/foreignkeys.html

current working order:  
	* create_racer_db.py - creates the db and various tables  
	* seed_racer_db.py - filles the racers table with fake info   
	* seed_dens.py - populates the den to id matches  
	* gen_heats.py - Needs fixing. Will generate the list of races and trials with randomized racers for each den  
		** If fails, run again races and trials databases will not reset IDs so may need to recreate them  
	* run_races.py -- will simulate a race for the total races in the races table  
	

called from others:  
	* derbytimer.py -- GPIO pi implementation of timer  
	* simulate_derbytimer.py -- what it says  
	* clear_db_tables.py -- erases some tables does not reset ID counts though  
	* gen_stats.py -- nothing yet  



