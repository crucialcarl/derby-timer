import sqlite3
import random
import sys

def ClearTrialsTable():
	conn = sqlite3.connect('racers.db')
	conn.execute("DELETE from trials");
	conn.commit()
	conn.close()
	print "Table Trials Cleared"

def ClearRacesTable():
	conn = sqlite3.connect('racers.db')
	conn.execute("DELETE from races");
	conn.commit()
	conn.close()
	print "Table Races Cleared"

