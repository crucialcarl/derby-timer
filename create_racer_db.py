#!/usr/bin/python

import sqlite3

conn = sqlite3.connect('racers.db')
print "[*] Opened database successfully";


conn.execute('''CREATE TABLE dens
	(id 	INTEGER PRIMARY KEY AUTOINCREMENT,
	name 	TEXT	NOT NULL);''')

print "[*] dens table created successfully";

conn.execute('''CREATE TABLE racers
	(racer_id 	INTEGER PRIMARY KEY AUTOINCREMENT,
	racer_name	TEXT	NOT NULL,
	racer_age	INT	NOT NULL,
	racer_den	INT	NOT NULL, 
	FOREIGN KEY(racer_den) REFERENCES dens(id));''')

print "[*] racers table created successfully";

conn.execute('''CREATE TABLE races
	(id 	INTEGER PRIMARY KEY AUTOINCREMENT);''')

print "[*] races table created successfully";

conn.execute('''CREATE TABLE trials
	(id	INTEGER PRIMARY KEY AUTOINCREMENT, 
	racer_id	INTEGER,
	track		INTEGER,
	time		TEXT,
	race_id		INTEGER,
	FOREIGN KEY(racer_id) REFERENCES racers(racer_id),
	FOREIGN KEY(race_id) REFERENCES races(id));''');

print "[*] trials table created successfully";

conn.close()
print "[*] Closed database successfully";
