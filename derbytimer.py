import RPi.GPIO as GPIO
import time
from datetime import datetime

def runit():
	## OUR PINS
	reedpin = 16
	
	## GPIO pins are 11, 12, 13 so we use OFFSET of 10 to refer to them as 1,2,3
	OFFSET = 10
	
	## QTI Light threshold to "end" a lane
	THRESHOLD = 400

	## LANES - in case we want to update later
	LANES = 3


	GPIO.setmode(GPIO.BOARD)
	GPIO.setwarnings(False)
	GPIO.setup(reedpin,GPIO.IN)

	while 1:
		if GPIO.input(reedpin) == 1:
			race_on = 1
			break
	if race_on == 1:
		time_start = datetime.now()
		print "\n### RACE STARTED! ###\n"
		finished_cars = 0 
		lane_time = []
		for R in range(0,LANES + 1):
			lane_time.append(0) 

		while (finished_cars < LANES):
			for i in [1,2,3]:
				z = sensorduration(i + OFFSET)
				if z < THRESHOLD:
					if lane_time[i] == 0: 
						time_end = datetime.now()
						time_delta = time_end - time_start
						lane_time[i] = time_delta.total_seconds()
						finished_cars = finished_cars + 1
		for x in lane_time:
			if x != 0:
				print "Lane: " + str(lane_time.index(x)) + "  Time: " + str(x)
		
		print "\n### RACE ENDED! ###\n"
		return lane_time

def sensorduration(pin):
	count = 0 
	GPIO.setup(pin,GPIO.OUT)
        GPIO.output(pin,GPIO.HIGH)
	time.sleep(0.001)

	GPIO.setup(pin, GPIO.IN)
	
	while (GPIO.input(pin) == 1):
		count = count + 1
	return count

if __name__ == "__main__":
    main()
