import sqlite3
import random
import sys
from clear_db_tables import * 

def getRacersFromDen(den):
	# return a list of racers from a den
	conn = sqlite3.connect('racers.db')
	#print "Database opened successfully";
	getracers = conn.execute("SELECT racer_id from racers where racer_den = \"" + str(den) + "\"")
	allracers = getracers.fetchall()
	conn.close()

	allracers2 = [ ]	
	for racer in allracers:
		allracers2.append(racer[0])
	return (allracers2)

def randomList(a):
	# randomize a list
	b = []
	for i in range(len(a)):
		element = random.choice(a)
		a.remove(element)
		b.append(element)
	return b

def genRaceID():
	# connect to races table and create a new ID.  return that ID
	conn = sqlite3.connect('racers.db')
	cursor = conn.cursor()
	#print "Database opened successfully";
	cursor.execute("INSERT into races (id) values (NULL)");
	conn.commit()
	conn.close()
	return (cursor.lastrowid)

def CountHeats(Racers):
	# return number of heats needed for a provided number of racers 
	heatsNeeded = Racers / 3
	if Racers % 3 > 0:
		heatsNeeded = heatsNeeded + 1
	return heatsNeeded

def GetDens():
	# return number of dens
	conn = sqlite3.connect('racers.db')	
	cursor = conn.cursor()
	count = cursor.execute("SELECT count(*) from dens");
	result = cursor.fetchone()
	mycount = result[0]
	conn.close()
	return (mycount + 1)

def genTrial(racer_id, track, race_id):
	conn = sqlite3.connect('racers.db')
	cursor = conn.cursor()
	cursor.execute("INSERT into trials (id, racer_id, track, time, race_id) values (NULL, \"" + str(racer_id) + "\", \"" + str(track) + "\", NULL, \"" + str(race_id) + "\")");
	conn.commit()
	conn.close()
	return (cursor.lastrowid)

for i in range(1, GetDens()):
	conn = sqlite3.connect('racers.db')	
	cursor = conn.cursor()
	DenName = cursor.execute("SELECT name from dens where id = \"" + str(i) + "\"");
	DenName2 = cursor.fetchone()
	DenName = DenName2[0]
	conn.close()

	print "\n\nDen Name: " + str(DenName)

	CurrDen = getRacersFromDen(i)
	heatsNeeded = CountHeats(len(CurrDen))
	print "We have " + str(len(CurrDen)) + " cars.";

	racersLeft = CurrDen[:]
	lane1 = []
	lane2 = []
	lane3 = []
	lanesArray = []
	
	lanesArray.append(lane1)
	lanesArray.append(lane2)
	lanesArray.append(lane3)


	racersLeft = CurrDen[:]

	for x in range(1, (len(CurrDen)) + 1 ):
		thisHeat = [] 
		raceID = genRaceID()
		if (len(racersLeft) < 3):
			print "\nNot enough eligible racers"
			print "Racers Left : " + str(len(racersLeft))
			ClearTrialsTable()
			ClearRacesTable()
			sys.exit()
		
		print "\nHeat : " + str(x) + " ", 
		for curlane in range(0,3):

			print "\tLane : " + str(curlane+1) + " ", 
			carSelected = 0
			while carSelected == 0:
				car = random.choice(racersLeft)
				if car in thisHeat:
	#					print "Car already in this Heat"
					continue
				elif car not in lanesArray[curlane]:
					thisHeat.append(car)
					lanesArray[curlane].append(car)
					if car in lanesArray[0]:
						if car in lanesArray[1]:
							if car in lanesArray[2]:
								racersLeft.remove(car)
					print "\tCar: " + str(car) + " ", 
					carSelected = 1
					genTrial(car, curlane, raceID)
					break





