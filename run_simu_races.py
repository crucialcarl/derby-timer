from simulate_derbytimer import runit
import sqlite3

def GetRaces():
	conn = sqlite3.connect('racers.db')
	cur = conn.cursor()    
    	cur.execute("SELECT * FROM races")
    	rows = cur.fetchall()
	conn.close()
	races = [] 
	for row in rows:
		races.append(row[0])
	return races

def GetRacersForRace(a):
	conn = sqlite3.connect('racers.db')
	cur = conn.cursor()    
    	cur.execute("SELECT racer_id, track FROM trials where race_id = \"" + str(a) + "\"")
    	rows = cur.fetchall()
	conn.close()
	racers = [] 
	tracks = []
	for row in rows:
		racers.append(row[0])
		tracks.append(row[1])
	return racers, tracks

def GetRacerNameFromTrialAndTrack(trial,track):
	conn = sqlite3.connect('racers.db')
	cur = conn.cursor()    
    	cur.execute("select racer_name from racers join trials on racers.racer_id = trials.racer_id where trials.race_id = \"" + str(trial) + "\" and trials.track = \"" + str(track) + "\"")
    	rows = cur.fetchall()
	conn.close()
	name = rows[0]
	return name 

def GetRacerNameFromRacerID(racerID):
	conn = sqlite3.connect('racers.db')
	cur = conn.cursor()    
    	cur.execute("select racer_name from racers where racer_id = \"" + str(racerID) + "\"")
    	rows = cur.fetchall()
	conn.close()
	name = rows[0]
	return name 

def AddTrialTimeFromRaceAndLane(race, lane, time):
	conn = sqlite3.connect('racers.db')
	cur = conn.cursor()    
    	cur.execute("update trials set time = \"" + str(time) + "\" where race_id = \"" + str(race) + "\" AND track = \"" + str(lane) + "\"")
	conn.commit()
	conn.close()
	return 

races = GetRaces()	

for x in range(1,len(races)+1):
#for x in range(1,2):
	racers,tracks = GetRacersForRace(x)
	print "RACE : " + str(x)
	for z in range(0, len(tracks)):
		print "Lane: " + str(tracks[z] + 1), 
		print "Racer: " + str(GetRacerNameFromRacerID(racers[z])[0])
	
	lane_times = runit()
	print "Lane\tTime\tRacer"
	for z in range(0, len(tracks)):
		print str(tracks[z] + 1), 
		print "\t" + str(lane_times[z+1]),
		print "\t" + str(GetRacerNameFromRacerID(racers[z])[0])

	for z in range(0, len(tracks)):
		AddTrialTimeFromRaceAndLane(x, z, lane_times[z+1])
	

