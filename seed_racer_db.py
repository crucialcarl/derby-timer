import sqlite3
import random

names = [ "Nicola", "Karen", "Fiona", "Susan", "Claire", "Sharon", "Angela", "Gillian", "Julie", "Michelle", "Jacqueline", "Amanda", "Tracy", "Louise", "Jennifer", "Alison", "Sarah", "Donna", "Caroline", "Elaine", "Lynn", "Margaret", "Elizabeth", "Lesley", "Deborah", "Pauline", "Lorraine", "Laura", "Lisa", "Tracey", "Carol", "Linda", "Lorna", "Catherine", "Wendy", "Lynne", "Yvonne", "Pamela", "Kirsty", "Jane", "Emma", "Joanne", "Heather", "Suzanne", "Anne", "Diane", "Helen", "Victoria", "Dawn", "Mary", "Samantha", "Marie", "Kerry", "Ann", "Hazel", "Christine", "Gail", "Andrea", "Clare", "Sandra", "Shona", "Kathleen", "Paula", "Shirley", "Denise", "Melanie", "Patricia", "Audrey", "Ruth", "Jill", "Lee", "Leigh", "Catriona", "Rachel", "Morag", "Kirsten", "Kirsteen", "Katrina", "Joanna", "Lynsey", "Cheryl", "Debbie", "Maureen", "Janet", "Aileen", "Arlene", "Zoe", "Lindsay", "Stephanie", "Judith", "Mandy", "Jillian", "Mhairi", "Barbara", "Carolyn", "Gayle", "Maria", "Valerie", "Christina", "Marion" ] 
dens = [ "Tiger", "Wolf", "Bear", "Webelos 1", "Webelos 2" ] 

def randomList(a):
	b = []
	for i in range(len(a)):
		element = random.choice(a)
		a.remove(element)
		b.append(element)
	return b

conn = sqlite3.connect('racers.db')
print "Opened database successfully";

x = 0

mixed = randomList(names)
for name in mixed:
	age = random.randint(5,10)
	dennumb = random.randint(1,5)

	conn.execute("INSERT INTO racers (racer_id, racer_name, racer_age, racer_den) values (NULL,\"" + str(name) + "\", " + str(age) + ", \"" + str(dennumb) + "\");")
	x = x + 1

print str(x) + " Records created"

conn.commit()
conn.close()

print "Database closed successfully"


