#import RPi.GPIO as GPIO
import time
from datetime import datetime
import random


def runit():
	## OUR PINS
	reedpin = 16
	
	## GPIO pins are 11, 12, 13 so we use OFFSET of 10 to refer to them as 1,2,3
	OFFSET = 10
	
	## QTI Light threshold to "end" a lane
	THRESHOLD = 400

	## LANES - in case we want to update later
	LANES = 3


	#GPIO.setmode(GPIO.BOARD)
	#GPIO.setwarnings(False)
	#GPIO.setup(reedpin,GPIO.IN)

	#while 1:
#		if GPIO.input(reedpin) == 1:
	#raw_input("Press Enter to simulate reed switch and start race")
	time_start = datetime.now()
	print "\n### RACE STARTED! ###\n"
	finished_cars = 0 
	lane_time = []
	for R in range(0,LANES + 1):
		lane_time.append(0) 

	while (finished_cars < LANES):
		for i in [1,2,3]:
			z = sensorduration(i + OFFSET)
			if z < THRESHOLD:
				if lane_time[i] == 0: 
#						print "Lane: " + str(i)
#						print "count: " + str(z)
					time_end = datetime.now()
					time_delta = time_end - time_start
					lane_time[i] = time_delta.total_seconds()
					finished_cars = finished_cars + 1
#						print lane_time
#						print finished_cars
	for x in lane_time:
		if x != 0:
			print "Lane: " + str(lane_time.index(x)) + "  Time: " + str(x)
	
	print "\n### RACE ENDED! ###\n"
	#	break
	return lane_time
		

def sensorduration(pin):
	count = 0 
	#GPIO.setup(pin,GPIO.OUT)
        #GPIO.output(pin,GPIO.HIGH)
	#time.sleep(0.001)
	#GPIO.setup(pin, GPIO.IN)
	mylist = []
	for x in range(1, 7500):
		mylist.append(x)
	y = random.choice(mylist)
	z = 0
	while (z != y):
		z = random.choice(mylist)
		count = count + 1
	return count

if __name__ == "__main__":
    #main()
    runit()
